import pytest
from unittest.mock import MagicMock

from _pytest.python_api import raises

from Shop import ShoppingCart

class TestDiscount:
    @pytest.fixture(autouse=True)
    def setup(self):
        print("\n Setup function")
        yield
        print("\n Teardown function")

    @pytest.fixture()
    def discount(self):
        discount = ShoppingCart()
        return discount

    def test_calDiscount(self, discount):
        discount.add_customer("Sam", 3000)
        assert discount.calculate_amount("Sam") == 2250

    @pytest.fixture()
    def mock_open(self, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value=2250)
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        return mock_open

    def test_returnsCorrectValue(self, discount, mock_open, monkeypatch):
        mock_exists = MagicMock(return_value=True)
        monkeypatch.setattr("os.path.exists", mock_exists)
        result = discount.read_from_file("input")
        mock_open.assert_called_once_with("input", "r")
        assert result == 2250
        # assert 2250==discount.calculate_amount("Sam")

    def test_throwsExceptionWithBadFile(self, discount, mock_open, monkeypatch):
        mock_exists = MagicMock(return_value=False)
        monkeypatch.setattr("os.path.exists", mock_exists)
        with raises(Exception):
            result = discount.readFromFile("input_file")