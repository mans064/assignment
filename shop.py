class ShoppingCart:
    def __init__(self):
        self.costs = {}

    def add_customer(self, name, cost):
        customer_name = name
        item_price = cost
        self.costs[customer_name] = item_price

    def calculate_amount(self, name):
        original_price = self.costs[name]
        if original_price <= 2000:
            discount=original_price*0.05 # discount of 5%
        elif (original_price>2000) and (original_price<=5000):
            discount = original_price * 0.25  # discount of 25%
        elif (original_price>5000) and (original_price<=10000):
             discount = original_price * 0.35  # discount of 35%
        else:
            discount = original_price * 0.5  # discount of 35%

        selling_price = original_price - discount
        print(original_price)
        return selling_price

    def read_from_file(self, filename):
         infile = open(filename, "r")
         line = infile.readline()
         return line